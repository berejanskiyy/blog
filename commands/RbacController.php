<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 */
class RbacController extends Controller
{

    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        $admin = $auth->createRole('admin');
        $user = $auth->createRole('user');

        // запишем их в БД
        $auth->add($admin);
        $auth->add($user);

           // Создаем наше правило, которое позволит проверить автора новости
        $authorRule = new \app\rbac\AuthorRule;

        // Запишем его в БД
        $auth->add($authorRule);

        // Создаем разрешения. Например, просмотр админки DeleteArticle и редактирование новости UpdateArticle
        $deleteArticle = $auth->createPermission('delete');
        $deleteArticle->description = 'Delete article';

        $updateArticle = $auth->createPermission('update');
        $updateArticle->description = 'Update article';

          // Создадим еще новое разрешение «Редактирование собственной новости» и ассоциируем его с правилом AuthorRule
        $updateOwnArticle = $auth->createPermission('updateOwnArticle');
        $updateOwnArticle->description = 'Update own article';

        $deleteOwnArticle = $auth->createPermission('deleteOwnArticle');
        $deleteOwnArticle->description = 'Delete own article';

        $createArticle = $auth->createPermission('create');
        $createArticle->description = 'Create article';

        $updateOwnArticle->ruleName = $authorRule->name;
        $deleteOwnArticle->ruleName = $authorRule->name;

        // Запишем эти разрешения в БД
        $auth->add($deleteArticle);
        $auth->add($updateOwnArticle);
        $auth->add($deleteOwnArticle);
        $auth->add($updateArticle);
        $auth->add($createArticle);


        $auth->addChild($user, $updateOwnArticle);
        $auth->addChild($user, $deleteOwnArticle);
        $auth->addChild($user, $createArticle);

        $auth->addChild($admin, $user);
        $auth->addChild($admin, $updateArticle);
        $auth->addChild($admin, $deleteArticle);
        // Назначаем роль admin пользователю с ID 1
        $auth->assign($admin, 1);

    }
}