<?php


use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<!--main content start-->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php foreach($articles as $article):?>
                    <article class="post">
                        <div class="post-thumb">

                            <a href="<?= Url::toRoute(['article/view', 'id'=>$article->id]);?>" class="post-thumb-overlay text-center">
                                <div class="text-uppercase text-center">View Post</div>
                            </a>
                        </div>
                        <div class="post-content">
                            <header class="entry-header text-center text-uppercase">
                                <p style="float: left; font-size: small">Author: <?= $article->author->username ?></p>
                                <h1 class="entry-title"><a href="<?= Url::toRoute(['article/view', 'id'=>$article->id]);?>"><?= $article->title?></a></h1>

                            </header>
                            <div class="entry-content">
                                <p><?= $article->description?>
                                </p>


                                <div class="btn-continue-reading text-center text-uppercase">
                                    <a href="<?= Url::toRoute(['article/view', 'id'=>$article->id]);?>" class="more-link">Continue Reading</a>
                                </div>
                            </div>
                        </div>
                    </article>
                <?php endforeach; ?>

                <?php
                    echo LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
            </div>
        </div>
    </div>
</div>
<!-- end main content-->