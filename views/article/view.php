<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
?>

<!--main content start-->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <article class="post">
                    <div class="post-thumb">


                        <a href="<?= Url::toRoute(['article/view', 'id' => $model->id]); ?>"
                           class="post-thumb-overlay text-center">
                            <div class="text-uppercase text-center">View Post</div>
                        </a>
                    </div>
                    <div class="post-content">
                        <header class="entry-header text-center text-uppercase">

                            <h1 class="entry-title"><a
                                        href="<?= Url::toRoute(['article/view', 'id' => $model->id]); ?>"><?= $model->title ?></a>
                            </h1>
                            <? $model->content ?>

                        </header>
                        <div class="entry-content">

                            <p><?= $model->content ?></p><br>

                            <p>Author: <?= $model->author->username ?></p>

                            <div class="btn-continue-reading text-center text-uppercase">

                                  <li style="list-style-type: none;"><?php
                        if (Yii::$app->user->can('updateOwnArticle', ['article' => $model])) {
                            echo Html::a('Update', ['article/update', 'id' => $model->id], ['class' => 'more-link']);
                        }
                        if (Yii::$app->user->can('deleteOwnArticle', ['article' => $model])) {
                                          echo Html::a('Delete', ['article/delete', 'id' => $model->id], ['class' => 'more-link']);
                                      }
                        ?></li>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>
<!-- end main content-->

