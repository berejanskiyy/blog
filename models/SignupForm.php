<?php

namespace app\models;

use Yii;
use yii\base\Model;

class SignupForm extends Model
{
    public $username;
    public $repeat_password;
    public $password;

    public function rules()
    {
        return [
            [['username', 'repeat_password', 'password'], 'required'],
            [['username'], 'string'],
            ['repeat_password', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);

            $isSave = $user->save();
            $auth = Yii::$app->authManager;
            $new_user = $auth->getRole('user'); // Получаем роль user
            $auth->assign($new_user, $user->id);
            return $isSave;

        }
    }
}