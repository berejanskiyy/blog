<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\SignupForm;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;


class UserController extends Controller
{

    public function actionLogin()
    {

        $modelLoginForm = new LoginForm();
        if ($modelLoginForm->load(Yii::$app->request->post()) && $modelLoginForm->login()) {
            return $this->redirect(\Yii::$app->urlManager->createUrl("article/blog"));
        }

        // move to login page
        return $this->render('login', ['modelLoginForm' => $modelLoginForm]);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(\Yii::$app->urlManager->createUrl("user/login"));
    }

    public function actionSignup()
    {
        $modelSignupForm = new SignupForm();

        if (Yii::$app->request->isPost) {

            $modelSignupForm->load(Yii::$app->request->post());
            if ($modelSignupForm->signup()) {

                return $this->redirect(['user/login']);
            }
        }
        return $this->render('signup', ['modelSignupForm' => $modelSignupForm]);
    }

}
