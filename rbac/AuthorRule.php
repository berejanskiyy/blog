<?php

namespace app\rbac;

use Yii;
use yii\rbac\Rule;

class AuthorRule extends Rule
{
    public $name = 'isAuthor';

    /**
     * @param string|integer $user ID пользователя.
     * @param Item $item роль или разрешение с которым это правило ассоциировано
     * @param array $params параметры, переданные в ManagerInterface::checkAccess(), например при вызове проверки
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        //return isset($params['article']) ? $params['article']->user_id == $user : false;
        if (Yii::$app->user->getIdentity()->id == 1) {
            return true;
        } elseif (isset($params['article'])) {
            return $params['article']->user_id == $user;
        }
        return false;
    }
}