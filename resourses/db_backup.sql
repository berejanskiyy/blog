-- MySQL dump 10.13  Distrib 5.5.59, for debian-linux-gnu (x86_64)
--
-- Host: 172.17.0.1    Database: blog
-- ------------------------------------------------------
-- Server version	5.5.59-0ubuntu0.14.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (6,'<p>Fake Elon Musk successfully scams Twitter users out of cryptocurrency<br></p>','<p>People, please please please listen closely: Elon Musk is not giving away Ether. ￼</p><p>Scammers have swarmed Twitter in recent weeks in an attempt to trick people out of their hard-earned cryptocurrency, and as a particular tweet today makes clear they\'re having a lot of success. </p>','<p>People, please please please listen closely: Elon Musk is not giving away Ether. ￼</p><p>Scammers have swarmed Twitter in recent weeks in an attempt to trick people out of their hard-earned cryptocurrency, and as a particular tweet today makes clear they\'re having a lot of success.</p><p><span></span>That\'s when the scammers come in. Someone, with a Twitter account designed to mimic the famous person\'s, posts a reply promising free bitcoin, ether, or some other cryptocurrency. All you supposedly need to do to be on the receiving end of this potential bonanza is send a little ether to a provided address. </p><p>See where this is going?</p><p>\"To celebrate this, I\'m giving awaу 5,000 ЕTH to my followers,\" reads once such reply from @elonlmusk (notice the extra \"L\"). \"To pаrtiсipаte, just sеnd 0.5-1 ЕTH to the address bеlow and gеt 5-10 ЕTH back to the address you used for the transaсtion.\" </p>',4),(7,'<p>SpaceX tried to catch a big piece of rocket<span class=\"redactor-invisible-space\">.<br></span></p>','<p> <span></span>After SpaceX successfully blasted three satellites into space early Thursday morning, the company tried to catch the Falcon 9 rocket\'s expensive nosecone, also known as a fairing, on a ship in the Pacific Ocean. The vessel, named \"Mr. Steven,\" was outfitted with a giant net.<br></p>','<p><span></span>After SpaceX successfully blasted three satellites into space early Thursday morning, the company tried to catch the Falcon 9 rocket\'s expensive nosecone, also known as a fairing, on a ship in the Pacific Ocean. The vessel, named \"Mr. Steven,\" was outfitted with a giant net.</p><p>According to SpaceX CEO Elon Musk, the boat missed catching the fairing by \"a few hundred meters,\" but he thinks there\'s a simple fix: Bigger parachutes to better control the parachuting fairing.</p><p>The fairing, which sheltered and released the payload during the rocket launch, parachuted down to the Pacific Ocean. A GPS guidance system helped guide the parachute close to the long, outstretched metallic arms of the awaiting ship.</p><p>Assuming the nosecone is in good shape, SpaceX may still try and retrieve it from the water.</p>',4),(8,'<p>Samsung, Sony, Huawei and more will headline mobile\'s biggest show</p>','<p>Every product here is independently selected by Mashable journalists. If you buy something featured, we may earn an affiliate commission which helps support our work.</p>','<p>Every product here is independently selected by Mashable journalists. If you buy something featured, we may earn an affiliate commission which helps support our work.￼</p><p>If you\'ve been looking to buy a new Android phone, you might want to wait a few days. Mobile World Congress — the biggest mobile industry event of the year — starts on Feb. 26 in Barcelona, and that means we\'ll see a lot of new Androids. The good ones. </p><p>Of course, this is true every year, but this particular instance of the MWC appears to be even more news-packed then usual. We\'ll see new devices from Samsung, LG, Huawei, Sony, Alcatel, and Asus, to name a few. </p>',5),(9,'<p>YouTube\'s algorithm is hurting America far more than Russian trolls ever could</p>','<p>Remember when we used to think the greatest threat we faced from Artificial Intelligence was that it would become Skynet and launch our entire nuclear arsenal, wiping out the human race?</p>','<p>Remember when we used to think the greatest threat we faced from Artificial Intelligence was that it would become Skynet and launch our entire nuclear arsenal, wiping out the human race? ￼</p><p>Good times. </p><p>But on the evidence of YouTube\'s latest mess, AI doesn\'t even need to bother blowing us up — it can just dumb us down and push endless conspiracy theory videos on us. That way we\'ll get so confused about the truth, we won\'t take any action to stop killing ourselves.</p>',5);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('admin','1',1519290129),('user','2',1519290130),('user','3',1519290130),('user','4',1519290130),('user','5',1519302429);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('admin',1,NULL,NULL,NULL,1519290129,1519290129),('create',2,'Create article',NULL,NULL,1519290129,1519290129),('delete',2,'Delete article',NULL,NULL,1519290129,1519290129),('deleteOwnArticle',2,'Delete own article','isAuthor',NULL,1519290129,1519290129),('update',2,'Update article',NULL,NULL,1519290129,1519290129),('updateOwnArticle',2,'Update own article','isAuthor',NULL,1519290129,1519290129),('user',1,NULL,NULL,NULL,1519290129,1519290129);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('user','create'),('admin','delete'),('user','deleteOwnArticle'),('admin','update'),('user','updateOwnArticle'),('admin','user');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
INSERT INTO `auth_rule` VALUES ('isAuthor','O:19:\"app\\rbac\\AuthorRule\":3:{s:4:\"name\";s:8:\"isAuthor\";s:9:\"createdAt\";i:1519290129;s:9:\"updatedAt\";i:1519290129;}',1519290129,1519290129);
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1518507531),('m140506_102106_rbac_init',1519154854),('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1519154855),('m180208_140452_create_user_table',1519152909),('m180210_103933_create_article_table',1519152909);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `auth_key` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Alex','$2y$13$WF0aH0VnrpgytdBw7WwXBuXJudijs5arKSP6WI36CxBJWhgclZfEW','xB_hYVMlPXUozvaMvDZEMEXl9KzXxQ8x'),(2,'A','$2y$13$ZOfksGdxNcQGZNvCzW8phOG7mOfBcYbsPTCRkNsgTufDm2ghvCqOK','MWttQZ2_HJVhGI8MZ9WGvnsyjUxTbpey'),(3,'t','$2y$13$K5KiWeGr8nolKHGFTqou7OqlU9L.TKq.GGYdzqptIQk3ttoXgCWeq','GRs5aWgQCeyxO4idtgw7DE0Pn1sI9gVa'),(4,'Roma','$2y$13$bgil3D9oOaQP/eyXya/BgOPrMPbRzxMzafj/yTjlEIBYYHKNRNvgS','kj457JRGkU7e4SZXWqq3GEVuW7o5g7tQ'),(5,'Pasha','$2y$13$Hk8TWsHXc/NHKlq4jfrVreGU3AjxxQGhb8PX9yxnoy20mwWwmvv4a','RZ8wU0FQCGG_Eqs60wH-6VIxTPGo84nu');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-22 22:54:45
